#include <iostream>
#include <list>
#include <ctime>
#include <cmath>
#include <vector>
#include<algorithm>
using namespace std;



void pushToList(int n) {
  list<int> L;
	clock_t t;
	cout << "Creating a list of " << n << " elements...\n";
	for (int i=0; i<n; i++) L.push_back(rand()); 

	cout << "Pushing 1000 elements at the end...\n";
	t = clock();
	for (int i=0; i<100000; i++) L.push_back(rand()); 
	t = clock() - t;
	cout << "Elapsed time for pushing 100000 elements at the end of a " << n
	     << " element list : " << static_cast<float>(t)/CLOCKS_PER_SEC << endl;
	cout << "---------------------\n";
}

void bubblesort(int n, clock_t & t) {
    vector<int> vec;

    for (int i=0; i<n; i++) vec.push_back(rand());

    t = clock();
    int temp;
    bool finished = false;
    while (!finished)    {
       finished = true;
       for (int i = 0; i < n-1; i++) {
          if (vec[i] > vec[i+1]) {
             temp = vec[i];
             vec[i] = vec[i+1];
             vec[i+1] = temp;
             finished=false;
          }
        }
     }
    t = clock() - t;
}

void insertToList(int n) {
	vector<int> V;
	int r;
	vector<int>::iterator it;
	clock_t t;
	clock_t t2; 
	clock_t t3;
	cout << "Creating a list of " << n << " elements...\n";
	for (int i=0; i<n; i++) V.push_back(rand()); 
	//Time for the sort
	t = clock();

	sort(V.begin(),V.end());
	t = clock() - t;
	
	//Time for the search
	t2 = clock();
	for (int i=0; i<100000; i++) 
		binary_search(V.begin(),V.end(), rand());
	t2= clock() - t2;
	
	//Time for the bubble sort
	bubblesort(n,t3);

	//Displays the info
	cout << "Elapsed time for sorting " << n << " random numbers on the vector: "
	     << static_cast<float>(t)/CLOCKS_PER_SEC << endl;
	cout << "---------------------\n";	
	cout << "Elapsed time for searching a number on the sorted vector: "
             << static_cast<float>(t2)/CLOCKS_PER_SEC << endl;
	cout << "---------------------\n";
	cout << "Elapsed time for bubble sorting " << n << " random numbers an a vector: "
	     << static_cast<float>(t3)/CLOCKS_PER_SEC << endl;
        cout << "---------------------\n";
}



int main (int argc, char *argv[]) {
    if (argc < 2) {
    	cout << "Usage: " << argv[0] << " <input_size>\n";
    	return 1;
    }
    int n = atoi(argv[1]);

    //pushToList(n);
    insertToList(n);
    return 0;
}
